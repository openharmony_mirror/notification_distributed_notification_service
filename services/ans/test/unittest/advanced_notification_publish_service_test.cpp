/*
 * Copyright (c) 2024-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <chrono>
#include <functional>
#include <memory>
#include <thread>

#include "gtest/gtest.h"

#define private public
#include "advanced_notification_service.h"
#include "advanced_datashare_helper.h"
#include "ability_manager_errors.h"
#include "ans_inner_errors.h"
#include "ans_log_wrapper.h"
#include "accesstoken_kit.h"
#include "notification_preferences.h"
#include "notification_constant.h"
#include "ans_ut_constant.h"
#include "ans_dialog_host_client.h"
#include "mock_push_callback_stub.h"

extern void MockIsOsAccountExists(bool exists);

using namespace testing::ext;
using namespace OHOS::Security::AccessToken;

namespace OHOS {
namespace Notification {
extern void MockIsVerfyPermisson(bool isVerify);
extern void MockGetTokenTypeFlag(ATokenTypeEnum mockRet);
extern void MockIsSystemApp(bool isSystemApp);
class AnsPublishServiceTest : public testing::Test {
public:
    static void SetUpTestCase();
    static void TearDownTestCase();
    void SetUp();
    void TearDown();

private:
    void TestAddNotification(int notificationId, const sptr<NotificationBundleOption> &bundle);
    void RegisterPushCheck();

private:
    static sptr<AdvancedNotificationService> advancedNotificationService_;
};

sptr<AdvancedNotificationService> AnsPublishServiceTest::advancedNotificationService_ = nullptr;

void AnsPublishServiceTest::SetUpTestCase() {}

void AnsPublishServiceTest::TearDownTestCase() {}

void AnsPublishServiceTest::SetUp()
{
    GTEST_LOG_(INFO) << "SetUp start";

    advancedNotificationService_ = new (std::nothrow) AdvancedNotificationService();
    NotificationPreferences::GetInstance()->ClearNotificationInRestoreFactorySettings();
    advancedNotificationService_->CancelAll("");
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_NATIVE);
    MockIsSystemApp(true);
    GTEST_LOG_(INFO) << "SetUp end";
}

void AnsPublishServiceTest::TearDown()
{
    delete advancedNotificationService_;
    advancedNotificationService_ = nullptr;
    GTEST_LOG_(INFO) << "TearDown";
}

void AnsPublishServiceTest::TestAddNotification(int notificationId, const sptr<NotificationBundleOption> &bundle)
{
    auto slotType = NotificationConstant::SlotType::LIVE_VIEW;
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(slotType);
    request->SetOwnerUserId(1);
    request->SetCreatorUserId(1);
    request->SetOwnerBundleName("test");
    request->SetOwnerUid(0);
    request->SetNotificationId(notificationId);
    auto record = advancedNotificationService_->MakeNotificationRecord(request, bundle);
    auto ret = advancedNotificationService_->AssignToNotificationList(record);
}

void AnsPublishServiceTest::RegisterPushCheck()
{
    auto pushCallbackProxy = new (std::nothrow)MockPushCallBackStub();
    EXPECT_NE(pushCallbackProxy, nullptr);
    sptr<IRemoteObject> pushCallback = pushCallbackProxy->AsObject();
    sptr<NotificationCheckRequest> checkRequest = new (std::nothrow) NotificationCheckRequest();
    checkRequest->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    ASSERT_EQ(advancedNotificationService_->RegisterPushCallback(pushCallback, checkRequest), ERR_OK);
}

/**
 * @tc.name: Publish_00001
 * @tc.desc: Test Publish
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, Publish_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    std::string label = "";
    request->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    auto localLiveContent = std::make_shared<NotificationLocalLiveViewContent>();
    auto content = std::make_shared<NotificationContent>(localLiveContent);
    request->SetContent(content);
    request->SetCreatorUid(1);
    request->SetOwnerUid(1);
    MockIsOsAccountExists(true);

    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);
    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERR_ANS_NON_SYSTEM_APP);
}

/**
 * @tc.name: Publish_00002
 * @tc.desc: Test Publish
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, Publish_00002, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    std::string label = "";
    request->SetSlotType(NotificationConstant::SlotType::CONTENT_INFORMATION);
    request->SetRemoveAllowed(false);
    request->SetInProgress(true);
    auto normalContent = std::make_shared<NotificationNormalContent>();
    auto content = std::make_shared<NotificationContent>(normalContent);
    request->SetContent(content);

    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);

    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERR_OK);
}


/**
 * @tc.name: Publish_00003
 * @tc.desc: Publish live_view notification once
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, Publish_00003, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    std::string label = "";
    request->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    request->SetNotificationId(1);
    auto liveContent = std::make_shared<NotificationLiveViewContent>();
    auto content = std::make_shared<NotificationContent>(liveContent);
    request->SetContent(content);
    RegisterPushCheck();
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);

    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERR_OK);

    sptr<NotificationSlot> slot;
    NotificationConstant::SlotType slotType = NotificationConstant::SlotType::LIVE_VIEW;
    ret = advancedNotificationService_->GetSlotByType(slotType, slot);
    ASSERT_EQ(ret, (int)ERR_OK);
    ASSERT_EQ(1, slot->GetAuthorizedStatus());
    ASSERT_EQ(1, slot->GetAuthHintCnt());
}

/**
 * @tc.name: Publish_00004
 * @tc.desc: Publish live_view notification twice
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, Publish_00004, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    std::string label = "";
    request->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    request->SetNotificationId(1);
    auto liveContent = std::make_shared<NotificationLiveViewContent>();
    auto content = std::make_shared<NotificationContent>(liveContent);
    request->SetContent(content);
    RegisterPushCheck();

    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);

    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERR_OK);

    sptr<NotificationRequest> request2 = new (std::nothrow) NotificationRequest();
    request2->SetNotificationId(2);
    request2->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    request2->SetContent(content);
    ret = advancedNotificationService_->Publish(label, request2);
    ASSERT_EQ(ret, (int)ERR_OK);

    sptr<NotificationSlot> slot;
    NotificationConstant::SlotType slotType = NotificationConstant::SlotType::LIVE_VIEW;
    ret = advancedNotificationService_->GetSlotByType(slotType, slot);
    ASSERT_EQ(ret, (int)ERR_OK);
    ASSERT_EQ(0, slot->GetAuthorizedStatus());
    ASSERT_EQ(2, slot->GetAuthHintCnt());
}

/**
 * @tc.name: Publish_00005
 * @tc.desc: Publish test receiver user and checkUserExists is true
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, Publish_00005, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    std::string label = "";
    request->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    request->SetNotificationId(1);
    request->SetReceiverUserId(101);
    auto liveContent = std::make_shared<NotificationLiveViewContent>();
    auto content = std::make_shared<NotificationContent>(liveContent);
    request->SetContent(content);
    RegisterPushCheck();
    MockIsOsAccountExists(true);
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);

    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: Publish_00006
 * @tc.desc: Publish test receiver user and checkUserExists is false
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, Publish_00006, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    std::string label = "";
    request->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    request->SetNotificationId(1);
    request->SetReceiverUserId(101);
    auto liveContent = std::make_shared<NotificationLiveViewContent>();
    auto content = std::make_shared<NotificationContent>(liveContent);
    request->SetContent(content);
    MockIsOsAccountExists(false);

    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERROR_USER_NOT_EXIST);
}

/**
 * @tc.name: DeleteByBundle_00001
 * @tc.desc: Test DeleteByBundle
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DeleteByBundle_00001, Function | SmallTest | Level1)
{
    sptr<NotificationBundleOption> bundleOption = nullptr;
    auto ret = advancedNotificationService_->DeleteByBundle(bundleOption);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: DeleteByBundle_00002
 * @tc.desc: Test DeleteByBundle
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DeleteByBundle_00002, Function | SmallTest | Level1)
{
    sptr<NotificationBundleOption> bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    auto ret = advancedNotificationService_->DeleteByBundle(bundle);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: DeleteByBundle_00003
 * @tc.desc: Test DeleteByBundle
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DeleteByBundle_00003, Function | SmallTest | Level1)
{
    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    TestAddNotification(1, bundle);
    auto ret = advancedNotificationService_->DeleteByBundle(bundle);
    ASSERT_EQ(ret, (int)ERR_OK);
    ASSERT_EQ(advancedNotificationService_->notificationList_.size(), 0);
}

/**
 * @tc.name: DeleteAll_00001
 * @tc.desc: Test DeleteAll
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DeleteAll_00001, Function | SmallTest | Level1)
{
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    auto ret = advancedNotificationService_->DeleteAll();
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: SetShowBadgeEnabledForBundle_00001
 * @tc.desc: Test SetShowBadgeEnabledForBundle
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, SetShowBadgeEnabledForBundle_00001, Function | SmallTest | Level1)
{
    sptr<NotificationBundleOption> bundleOption = nullptr;
    auto ret = advancedNotificationService_->SetShowBadgeEnabledForBundle(bundleOption, true);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);

    bool enabled = false;
    ret = advancedNotificationService_->GetShowBadgeEnabledForBundle(bundleOption, enabled);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: SetShowBadgeEnabledForBundle_00002
 * @tc.desc: Test SetShowBadgeEnabledForBundle
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, SetShowBadgeEnabledForBundle_00002, Function | SmallTest | Level1)
{
    sptr<NotificationBundleOption> bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    auto ret = advancedNotificationService_->SetShowBadgeEnabledForBundle(bundle, true);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    bool enabled = false;
    ret = advancedNotificationService_->GetShowBadgeEnabledForBundle(bundle, enabled);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: GetShowBadgeEnabled_00001
 * @tc.desc: Test GetShowBadgeEnabled
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, GetShowBadgeEnabled_00001, Function | SmallTest | Level1)
{
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    bool enabled = false;
    auto ret = advancedNotificationService_->GetShowBadgeEnabled(enabled);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: GetShowBadgeEnabled_00002
 * @tc.desc: Test GetShowBadgeEnabled
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, GetShowBadgeEnabled_00002, Function | SmallTest | Level1)
{
    bool enabled = true;
    MockGetTokenTypeFlag(ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    auto ret = advancedNotificationService_->GetShowBadgeEnabled(enabled);
    ASSERT_EQ(ret, (int)ERR_OK);
    ASSERT_EQ(enabled, true);
}

/**
 * @tc.name: RequestEnableNotification_00001
 * @tc.desc: Test RequestEnableNotification
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RequestEnableNotification_00001, Function | SmallTest | Level1)
{
    std::string deviceId = "deviceId";
    sptr<AnsDialogHostClient> client = nullptr;
    AnsDialogHostClient::CreateIfNullptr(client);
    client = AnsDialogHostClient::GetInstance();
    sptr<IRemoteObject> callerToken = nullptr;

    auto ret = advancedNotificationService_->SetNotificationsEnabledForAllBundles(std::string(), false);
    ASSERT_EQ(ret, (int)ERR_OK);

    ret = advancedNotificationService_->RequestEnableNotification(deviceId, client, callerToken);
    ASSERT_EQ(ret, (int)ERROR_INTERNAL_ERROR);
}

/**
 * @tc.name: RequestEnableNotification_00002
 * @tc.desc: Test RequestEnableNotification
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RequestEnableNotification_00002, Function | SmallTest | Level1)
{
    std::string deviceId = "deviceId";
    sptr<AnsDialogHostClient> client = nullptr;
    AnsDialogHostClient::CreateIfNullptr(client);
    client = AnsDialogHostClient::GetInstance();
    sptr<IRemoteObject> callerToken = nullptr;

    auto ret = advancedNotificationService_->SetNotificationsEnabledForAllBundles(std::string(), true);
    ASSERT_EQ(ret, (int)ERR_OK);

    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    ret = advancedNotificationService_->RequestEnableNotification(deviceId, client, callerToken);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: RequestEnableNotification_00003
 * @tc.desc: Test RequestEnableNotification
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RequestEnableNotification_00003, Function | SmallTest | Level1)
{
    std::string deviceId = "deviceId";
    sptr<AnsDialogHostClient> client = nullptr;
    AnsDialogHostClient::CreateIfNullptr(client);
    client = AnsDialogHostClient::GetInstance();
    sptr<IRemoteObject> callerToken = nullptr;

    auto ret = advancedNotificationService_->SetNotificationsEnabledForAllBundles(std::string(), false);
    ASSERT_EQ(ret, (int)ERR_OK);

    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);

    auto bundle = advancedNotificationService_->GenerateBundleOption();
    NotificationPreferences::GetInstance()->SetHasPoppedDialog(bundle, true);

    ret = advancedNotificationService_->RequestEnableNotification(deviceId, client, callerToken);
    ASSERT_EQ(ret, (int)ERR_ANS_NOT_ALLOWED);

    NotificationPreferences::GetInstance()->SetHasPoppedDialog(bundle, false);
    ret = advancedNotificationService_->RequestEnableNotification(deviceId, client, callerToken);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: RequestEnableNotification_00004
 * @tc.desc: Test RequestEnableNotification,two parameters,except is ERROR_INTERNAL_ERROR
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RequestEnableNotification_00004, Function | SmallTest | Level1)
{
    std::string bundleName = "bundleName1";
    int32_t uid = 1;
    auto ret = advancedNotificationService_->RequestEnableNotification(bundleName, uid);
    ASSERT_EQ(ret, (int)ERROR_INTERNAL_ERROR);
}

/**
 * @tc.name: SetNotificationsEnabledForAllBundles_00001
 * @tc.desc: Test SetNotificationsEnabledForAllBundles
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, SetNotificationsEnabledForAllBundles_00001, Function | SmallTest | Level1)
{
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    bool enabled = false;
    auto ret = advancedNotificationService_->SetNotificationsEnabledForAllBundles("", enabled);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: SetNotificationsEnabledForSpecialBundle_00001
 * @tc.desc: Test SetNotificationsEnabledForSpecialBundle
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, SetNotificationsEnabledForSpecialBundle_00001, Function | SmallTest | Level1)
{
    sptr<NotificationBundleOption> bundle = nullptr;
    bool enabled = false;
    std::string deviceId = "deviceId";
    auto ret = advancedNotificationService_->SetNotificationsEnabledForSpecialBundle(deviceId, bundle, enabled);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: IsAllowedNotify_00001
 * @tc.desc: Test IsAllowedNotify
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, IsAllowedNotify_00001, Function | SmallTest | Level1)
{
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    bool allowed = false;
    auto ret = advancedNotificationService_->IsAllowedNotify(allowed);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: IsAllowedNotifyForBundle_00001
 * @tc.desc: Test IsAllowedNotifyForBundle
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, IsAllowedNotifyForBundle_00001, Function | SmallTest | Level1)
{
    bool allowed = false;
    sptr<NotificationBundleOption> bundle = nullptr;
    auto ret = advancedNotificationService_->IsAllowedNotifyForBundle(bundle, allowed);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: TriggerLocalLiveView_00001
 * @tc.desc: Test TriggerLocalLiveView
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, TriggerLocalLiveView_00001, Function | SmallTest | Level1)
{
    int notificationId = 1;
    sptr<NotificationBundleOption> bundle = nullptr;
    sptr<NotificationButtonOption> buttonOption = nullptr;

    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);
    MockIsVerfyPermisson(false);

    auto ret = advancedNotificationService_->TriggerLocalLiveView(bundle, notificationId, buttonOption);
    ASSERT_EQ(ret, (int)ERR_ANS_NON_SYSTEM_APP);

    MockIsSystemApp(true);
    ret = advancedNotificationService_->TriggerLocalLiveView(bundle, notificationId, buttonOption);
    ASSERT_EQ(ret, (int)ERR_ANS_PERMISSION_DENIED);

    MockIsVerfyPermisson(true);
    ret = advancedNotificationService_->TriggerLocalLiveView(bundle, notificationId, buttonOption);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: RemoveNotification_00001
 * @tc.desc: Test RemoveNotification
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveNotification_00001, Function | SmallTest | Level1)
{
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    std::string label = "label";
    int notificationId = 1;
    auto ret = advancedNotificationService_->RemoveNotification(bundle, notificationId, label, 0);

    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: RemoveNotifications_00001
 * @tc.desc: Test RemoveNotifications
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveNotifications_00001, Function | SmallTest | Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);
    std::vector<std::string> keys;
    int removeReason = 1;
    auto ret = advancedNotificationService_->RemoveNotifications(keys, removeReason);
    ASSERT_EQ(ret, (int)ERR_ANS_NON_SYSTEM_APP);

    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);
    ret = advancedNotificationService_->RemoveNotifications(keys, removeReason);
    ASSERT_EQ(ret, (int)ERR_ANS_PERMISSION_DENIED);

    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    MockIsVerfyPermisson(true);
    ret = advancedNotificationService_->RemoveNotifications(keys, removeReason);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: RemoveNotifications_00002
 * @tc.desc: Test RemoveNotifications
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveNotifications_00002, Function | SmallTest | Level1)
{
    std::vector<std::string> keys;
    int removeReason = 1;

    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    sptr<NotificationRequest> req = new (std::nothrow) NotificationRequest();
    req->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    req->SetOwnerUserId(1);
    req->SetOwnerBundleName(TEST_DEFUALT_BUNDLE);
    req->SetNotificationId(1);
    auto record = advancedNotificationService_->MakeNotificationRecord(req, bundle);
    auto ret = advancedNotificationService_->AssignToNotificationList(record);
    keys.emplace_back(record->notification->GetKey());

    ret = advancedNotificationService_->RemoveNotifications(keys, removeReason);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: RemoveNotificationBySlot_00001
 * @tc.desc: Test RemoveNotificationBySlot
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveNotificationBySlot_00001, Function | SmallTest | Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);
    sptr<NotificationBundleOption> bundle = nullptr;
    sptr<NotificationSlot> slot = nullptr;
    auto ret = advancedNotificationService_->RemoveNotificationBySlot(bundle, slot,
        NotificationConstant::DEFAULT_REASON_DELETE);
    ASSERT_EQ(ret, (int)ERR_ANS_NON_SYSTEM_APP);

    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    ret = advancedNotificationService_->RemoveNotificationBySlot(bundle, slot,
        NotificationConstant::DEFAULT_REASON_DELETE);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_BUNDLE);
}

/**
 * @tc.name: RemoveNotificationBySlot_00002
 * @tc.desc: Test RemoveNotificationBySlot
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveNotificationBySlot_00002, Function | SmallTest | Level1)
{
    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    sptr<NotificationRequest> req = new (std::nothrow) NotificationRequest();
    req->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    req->SetOwnerUserId(1);
    req->SetOwnerBundleName(TEST_DEFUALT_BUNDLE);
    req->SetNotificationId(1);
    auto multiLineContent = std::make_shared<NotificationMultiLineContent>();
    auto content = std::make_shared<NotificationContent>(multiLineContent);
    req->SetContent(content);
    auto record = advancedNotificationService_->MakeNotificationRecord(req, bundle);
    auto ret = advancedNotificationService_->AssignToNotificationList(record);
    auto slot = new NotificationSlot(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);

    ret = advancedNotificationService_->RemoveNotificationBySlot(bundle, slot,
        NotificationConstant::DEFAULT_REASON_DELETE);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: NotificationSvrQueue_00001
 * @tc.desc: Test notificationSvrQueue is nullptr
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, NotificationSvrQueue_00001, Function | SmallTest | Level1)
{
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    advancedNotificationService_->notificationSvrQueue_ = nullptr;
    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);

    auto ret = advancedNotificationService_->CancelAll("");
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    ret = advancedNotificationService_->Delete("", 1);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    ret = advancedNotificationService_->CancelGroup("group", "");
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    ret = advancedNotificationService_->RemoveGroupByBundle(bundle, "group");
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    bool allowed = false;
    ret = advancedNotificationService_->IsSpecialUserAllowedNotify(1, allowed);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    ret = advancedNotificationService_->SetNotificationsEnabledByUser(1, false);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    ret = advancedNotificationService_->SetBadgeNumber(1, "");
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);

    ret = advancedNotificationService_->SubscribeLocalLiveView(nullptr, nullptr, true);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/*
 * @tc.name: SetDistributedEnabledByBundle_0100
 * @tc.desc: test SetDistributedEnabledByBundle with parameters
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, SetDistributedEnabledByBundle_0100, TestSize.Level1)
{
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    sptr<NotificationBundleOption> bundleOption(new NotificationBundleOption("bundleName", 1));
    std::string deviceType = "testDeviceType";

    ErrCode res = advancedNotificationService_->SetDistributedEnabledByBundle(bundleOption, deviceType, true);
    ASSERT_EQ(res, ERR_OK);
}

/*
 * @tc.name: SetDistributedEnabledByBundle_0200
 * @tc.desc: test SetDistributedEnabledByBundle with parameters, expect errorCode ERR_ANS_NON_SYSTEM_APP.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, SetDistributedEnabledByBundle_0200, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);
    sptr<NotificationBundleOption> bundleOption(new NotificationBundleOption("bundleName", 1));
    std::string deviceType = "testDeviceType";

    ErrCode res = advancedNotificationService_->SetDistributedEnabledByBundle(bundleOption, deviceType, true);
    ASSERT_EQ(res, ERR_ANS_NON_SYSTEM_APP);
}

/*
 * @tc.name: SetDistributedEnabledByBundle_0300
 * @tc.desc: test SetDistributedEnabledByBundle with parameters, expect errorCode ERR_ANS_PERMISSION_DENIED.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, SetDistributedEnabledByBundle_0300, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);
    sptr<NotificationBundleOption> bundleOption(new NotificationBundleOption("bundleName", 1));
    std::string deviceType = "testDeviceType";

    ErrCode res = advancedNotificationService_->SetDistributedEnabledByBundle(bundleOption, deviceType, true);
    ASSERT_EQ(res, ERR_ANS_PERMISSION_DENIED);
}


/**
 * @tc.name: IsDistributedEnabledByBundle_0100
 * @tc.desc: test IsDistributedEnabledByBundle with parameters
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsDistributedEnabledByBundle_0100, TestSize.Level1)
{
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    sptr<NotificationBundleOption> bundleOption(new NotificationBundleOption("bundleName", 1));
    std::string deviceType = "testDeviceType1111";
    bool enable = true;
    ErrCode result = advancedNotificationService_->IsDistributedEnabledByBundle(bundleOption, deviceType, enable);
    ASSERT_EQ(result, ERR_OK);
}

/**
 * @tc.name: IsDistributedEnabledByBundle_0200
 * @tc.desc: test IsDistributedEnabledByBundle with parameters
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsDistributedEnabledByBundle_0200, TestSize.Level1)
{
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    sptr<NotificationBundleOption> bundleOption(new NotificationBundleOption("bundleName", 1));
    std::string deviceType = "testDeviceType";

    ErrCode ret = advancedNotificationService_->SetDistributedEnabledByBundle(bundleOption, deviceType, true);
    ASSERT_EQ(ret, ERR_OK);
    bool enable = false;
    ret = advancedNotificationService_->IsDistributedEnabledByBundle(bundleOption, deviceType, enable);
    ASSERT_EQ(ret, ERR_OK);
    ASSERT_EQ(enable, true);
}

/**
 * @tc.name: IsDistributedEnabledByBundle_0300
 * @tc.desc: test IsDistributedEnabledByBundle with parameters, expect errorCode ERR_ANS_NON_SYSTEM_APP.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsDistributedEnabledByBundle_0300, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);
    sptr<NotificationBundleOption> bundleOption(new NotificationBundleOption("bundleName", 1));
    std::string deviceType = "testDeviceType1111";
    bool enable = true;
    ErrCode result = advancedNotificationService_->IsDistributedEnabledByBundle(bundleOption, deviceType, enable);
    ASSERT_EQ(result, ERR_ANS_NON_SYSTEM_APP);
}

/**
 * @tc.name: IsDistributedEnabledByBundle_0400
 * @tc.desc: test IsDistributedEnabledByBundle with parameters, expect errorCode ERR_ANS_PERMISSION_DENIED.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsDistributedEnabledByBundle_0400, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);
    sptr<NotificationBundleOption> bundleOption(new NotificationBundleOption("bundleName", 1));
    std::string deviceType = "testDeviceType1111";
    bool enable = true;
    ErrCode result = advancedNotificationService_->IsDistributedEnabledByBundle(bundleOption, deviceType, enable);
    ASSERT_EQ(result, ERR_ANS_PERMISSION_DENIED);
}

/**
 * @tc.name: DuplicateMsgControl_00001
 * @tc.desc: Test DuplicateMsgControl
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DuplicateMsgControl_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    auto liveViewContent = std::make_shared<NotificationLiveViewContent>();
    auto content = std::make_shared<NotificationContent>(liveViewContent);
    request->SetContent(content);

    auto ret = advancedNotificationService_->DuplicateMsgControl(request);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: DuplicateMsgControl_00002
 * @tc.desc: Test DuplicateMsgControl
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DuplicateMsgControl_00002, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    request->SetAppMessageId("test1");
    auto uniqueKey = request->GenerateUniqueKey();
    advancedNotificationService_->uniqueKeyList_.emplace_back(
        std::make_pair(std::chrono::system_clock::now(), uniqueKey));

    auto ret = advancedNotificationService_->DuplicateMsgControl(request);
    ASSERT_EQ(ret, (int)ERR_ANS_DUPLICATE_MSG);
}

/**
 * @tc.name: DuplicateMsgControl_00003
 * @tc.desc: Test DuplicateMsgControl
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DuplicateMsgControl_00003, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    request->SetAppMessageId("test2");

    auto ret = advancedNotificationService_->DuplicateMsgControl(request);
    ASSERT_EQ(ret, (int)ERR_OK);
    ASSERT_EQ(advancedNotificationService_->uniqueKeyList_.size(), 1);
}

/**
 * @tc.name: IsDuplicateMsg_00001
 * @tc.desc: Test IsDuplicateMsg
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, IsDuplicateMsg_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    request->SetAppMessageId("test2");
    auto uniqueKey = request->GenerateUniqueKey();

    auto ret = advancedNotificationService_->IsDuplicateMsg(uniqueKey);
    ASSERT_EQ(ret, false);
}

/**
 * @tc.name: IsDuplicateMsg_00002
 * @tc.desc: Test IsDuplicateMsg
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, IsDuplicateMsg_00002, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    request->SetAppMessageId("test2");
    auto uniqueKey = request->GenerateUniqueKey();
    advancedNotificationService_->uniqueKeyList_.emplace_back(
        std::make_pair(std::chrono::system_clock::now(), uniqueKey));

    auto ret = advancedNotificationService_->IsDuplicateMsg(uniqueKey);
    ASSERT_EQ(ret, true);
}

/**
 * @tc.name: RemoveExpiredUniqueKey_00001
 * @tc.desc: Test RemoveExpiredUniqueKey
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveExpiredUniqueKey_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    request->SetAppMessageId("test2");
    auto uniqueKey = request->GenerateUniqueKey();
    advancedNotificationService_->uniqueKeyList_.emplace_back(
        std::make_pair(std::chrono::system_clock::now() - std::chrono::hours(24), uniqueKey));

    sleep(1);
    ASSERT_EQ(advancedNotificationService_->uniqueKeyList_.size(), 1);
    advancedNotificationService_->RemoveExpiredUniqueKey();
    ASSERT_EQ(advancedNotificationService_->uniqueKeyList_.size(), 0);
}

/*
 * @tc.name: SetSmartReminderEnabled_0100
 * @tc.desc: test SetSmartReminderEnabled with parameters
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, SetSmartReminderEnabled_0100, TestSize.Level1)
{
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    ErrCode res = advancedNotificationService_->SetSmartReminderEnabled("testDeviceType", true);
    ASSERT_EQ(res, ERR_OK);
}

/*
 * @tc.name: SetSmartReminderEnabled_0200
 * @tc.desc: test SetSmartReminderEnabled with parameters, expect errorCode ERR_ANS_NON_SYSTEM_APP.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, SetSmartReminderEnabled_0200, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);

    ErrCode res = advancedNotificationService_->SetSmartReminderEnabled("testDeviceType", true);
    ASSERT_EQ(res, ERR_ANS_NON_SYSTEM_APP);
}

/*
 * @tc.name: SetSmartReminderEnabled_0300
 * @tc.desc: test SetSmartReminderEnabled with parameters, expect errorCode ERR_ANS_PERMISSION_DENIED.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, SetSmartReminderEnabled_0300, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);

    ErrCode res = advancedNotificationService_->SetSmartReminderEnabled("testDeviceType", true);
    ASSERT_EQ(res, ERR_ANS_PERMISSION_DENIED);
}


/**
 * @tc.name: IsSmartReminderEnabled_0100
 * @tc.desc: test IsSmartReminderEnabled with parameters
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsSmartReminderEnabled_0100, TestSize.Level1)
{
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    bool enable = true;
    ErrCode result = advancedNotificationService_->IsSmartReminderEnabled("testDeviceType1111", enable);
    ASSERT_EQ(result, ERR_OK);
}

/**
 * @tc.name: IsSmartReminderEnabled_0200
 * @tc.desc: test IsSmartReminderEnabled with parameters
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsSmartReminderEnabled_0200, TestSize.Level1)
{
    MockIsSystemApp(true);
    MockIsVerfyPermisson(true);
    ErrCode ret = advancedNotificationService_->SetSmartReminderEnabled("testDeviceType", true);
    ASSERT_EQ(ret, ERR_OK);
    bool enable = false;
    ret = advancedNotificationService_->IsSmartReminderEnabled("testDeviceType", enable);
    ASSERT_EQ(ret, ERR_OK);
    ASSERT_EQ(enable, true);
}

/**
 * @tc.name: IsSmartReminderEnabled_0300
 * @tc.desc: test IsSmartReminderEnabled with parameters, expect errorCode ERR_ANS_NON_SYSTEM_APP.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsSmartReminderEnabled_0300, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(false);
    bool enable = true;
    ErrCode result = advancedNotificationService_->IsSmartReminderEnabled("testDeviceType1111", enable);
    ASSERT_EQ(result, ERR_ANS_NON_SYSTEM_APP);
}

/**
 * @tc.name: IsSmartReminderEnabled_0400
 * @tc.desc: test IsSmartReminderEnabled with parameters, expect errorCode ERR_ANS_PERMISSION_DENIED.
 * @tc.type: FUNC
 */
HWTEST_F(AnsPublishServiceTest, IsSmartReminderEnabled_0400, TestSize.Level1)
{
    MockGetTokenTypeFlag(Security::AccessToken::ATokenTypeEnum::TOKEN_HAP);
    MockIsSystemApp(true);
    MockIsVerfyPermisson(false);
    bool enable = true;
    ErrCode result = advancedNotificationService_->IsSmartReminderEnabled("testDeviceType1111", enable);
    ASSERT_EQ(result, ERR_ANS_PERMISSION_DENIED);
}

/**
 * @tc.name: PublishRemoveDuplicateEvent_00001
 * @tc.desc: Test PublishRemoveDuplicateEvent
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, PublishRemoveDuplicateEvent_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    request->SetAppMessageId("test2");
    request->SetNotificationId(1);
    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    auto record = advancedNotificationService_->MakeNotificationRecord(request, bundle);

    auto ret = advancedNotificationService_->PublishRemoveDuplicateEvent(record);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: PublishRemoveDuplicateEvent_00002
 * @tc.desc: Test PublishRemoveDuplicateEvent
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, PublishRemoveDuplicateEvent_00002, Function | SmallTest | Level1)
{
    std::shared_ptr<NotificationRecord> record= nullptr;
    auto ret = advancedNotificationService_->PublishRemoveDuplicateEvent(record);
    ASSERT_EQ(ret, (int)ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: PublishRemoveDuplicateEvent_00003
 * @tc.desc: Test PublishRemoveDuplicateEvent
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, PublishRemoveDuplicateEvent_00003, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    request->SetAppMessageId("test2");
    request->SetNotificationId(1);
    request->SetIsAgentNotification(true);
    auto normalContent = std::make_shared<NotificationNormalContent>();
    auto content = std::make_shared<NotificationContent>(normalContent);
    request->SetContent(content);
    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    auto record = advancedNotificationService_->MakeNotificationRecord(request, bundle);

    auto ret = advancedNotificationService_->PublishRemoveDuplicateEvent(record);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: CanPopEnableNotificationDialog_001
 * @tc.desc: Test CanPopEnableNotificationDialog
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, CanPopEnableNotificationDialog_001, Function | SmallTest | Level1)
{
    sptr<IAnsDialogCallback> callback = nullptr;
    bool canPop = false;
    std::string bundleName = "";
    ErrCode result = advancedNotificationService_->CanPopEnableNotificationDialog(callback, canPop, bundleName);
    ASSERT_EQ(result, ERROR_INTERNAL_ERROR);
}

/**
 * @tc.name: IsDisableNotification_001
 * @tc.desc: Test IsDisableNotification
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, IsDisableNotification_001, Function | SmallTest | Level1)
{
    std::string bundleName = "";
    bool result = advancedNotificationService_->IsDisableNotification(bundleName);
    ASSERT_FALSE(result);
}

/**
 * @tc.name: IsNeedToControllerByDisableNotification_001
 * @tc.desc: Test IsNeedToControllerByDisableNotification
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, IsNeedToControllerByDisableNotification_001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    bool result = advancedNotificationService_->IsNeedToControllerByDisableNotification(request);
    EXPECT_TRUE(result);
}

/**
 * @tc.name: IsNeedToControllerByDisableNotification_002
 * @tc.desc: Test IsNeedToControllerByDisableNotification
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, IsNeedToControllerByDisableNotification_002, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = nullptr;
    bool result = advancedNotificationService_->IsNeedToControllerByDisableNotification(request);
    EXPECT_FALSE(result);
}

/**
 * @tc.name: PrePublishRequest_00001
 * @tc.desc: Test PrePublishRequest
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, PrePublishRequest_00001, Function | SmallTest | Level1)
{
    MockIsOsAccountExists(false);
    sptr<NotificationRequest> request = new NotificationRequest();
    request->SetReceiverUserId(-99);
    ASSERT_EQ(advancedNotificationService_->PrePublishRequest(request), (int)ERROR_USER_NOT_EXIST);
    MockIsOsAccountExists(true);
    sptr<NotificationRequest> request1 = new NotificationRequest();
    request1->SetCreatorUid(0);
    request1->SetReceiverUserId(100);
    ASSERT_EQ(advancedNotificationService_->PrePublishRequest(request1), (int)ERR_ANS_INVALID_UID);
    sptr<NotificationRequest> request2 = new NotificationRequest();
    request2->SetDeliveryTime(-1);
    request2->SetReceiverUserId(100);
    request2->SetCreatorUid(1);
    ASSERT_EQ(advancedNotificationService_->PrePublishRequest(request2), (int)ERR_OK);
}

/**
 * @tc.name: CollaboratePublish_00001
 * @tc.desc: Test CollaboratePublish
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, CollaboratePublish_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::SOCIAL_COMMUNICATION);
    std::string label = "";
    request->SetAppMessageId("test2");
    request->SetNotificationId(1);
    request->SetIsAgentNotification(true);
    request->SetDistributedCollaborate(true);
    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: CollaboratePublish_00002
 * @tc.desc: Test CollaboratePublish, common live view, except is ERR_OK
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, CollaboratePublish_00002, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSlotType(NotificationConstant::SlotType::LIVE_VIEW);
    std::shared_ptr<NotificationLiveViewContent> liveViewContent = std::make_shared<NotificationLiveViewContent>();
    std::shared_ptr<NotificationContent> notificationContent = std::make_shared<NotificationContent>(liveViewContent);
    request->SetContent(notificationContent);
    std::string label = "";
    request->SetAppMessageId("test1");
    request->SetNotificationId(1);
    request->SetIsAgentNotification(true);
    request->SetDistributedCollaborate(true);
    auto ret = advancedNotificationService_->Publish(label, request);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: PublishNotificationForIndirectProxy_00001
 * @tc.desc: Test PublishNotificationForIndirectProxy, except is ERR_OK
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, PublishNotificationForIndirectProxy_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    request->SetSound("sound");
    request->SetCreatorBundleName("creatorname");
    request->SetCreatorUid(1);
    request->SetAppInstanceKey("key");
    auto ret = advancedNotificationService_->PublishNotificationForIndirectProxy(request);
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: RemoveEnableNotificationDialog_00001
 * @tc.desc: Test RemoveEnableNotificationDialog, except is ERR_OK
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveEnableNotificationDialog_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    auto ret = advancedNotificationService_->RemoveEnableNotificationDialog();
    ASSERT_EQ(ret, (int)ERR_OK);
}

/**
 * @tc.name: QueryContactByProfileId_00001
 * @tc.desc: Test QueryContactByProfileId, except is -1
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, QueryContactByProfileId_00001, Function | SmallTest | Level1)
{
    auto datashareHelper = DelayedSingleton<AdvancedDatashareHelper>::GetInstance();
    bool isDataShareReady = true;
    datashareHelper->SetIsDataShareReady(isDataShareReady);
    std::string phoneNumber = "12345678";
    std::string policy = "5";
    int32_t userId = 100;
    auto ret = advancedNotificationService_->QueryContactByProfileId(phoneNumber, policy, userId);
    ASSERT_EQ(ret, -1);
}

/**
 * @tc.name: SetDistributedEnabledBySlot_00001
 * @tc.desc: Test SetDistributedEnabledBySlot, except is -1
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, SetDistributedEnabledBySlot_00001, Function | SmallTest | Level1)
{
    NotificationConstant::SlotType slotType = NotificationConstant::SlotType::SOCIAL_COMMUNICATION;
    std::string deviceType = "testdeviceType";
    bool enabled = true;
    auto ret = advancedNotificationService_->SetDistributedEnabledBySlot(slotType, deviceType, enabled);
    ASSERT_EQ(ret, ERR_OK);
}

/**
 * @tc.name: SetTargetDeviceStatus_00001
 * @tc.desc: Test SetTargetDeviceStatus, deviceType is empty, except is ERR_ANS_INVALID_PARAM
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, SetTargetDeviceStatus_00001, Function | SmallTest | Level1)
{
    std::string deviceType = "";
    uint32_t status = 0;
    auto ret = advancedNotificationService_->SetTargetDeviceStatus(deviceType, status);
    ASSERT_EQ(ret, ERR_ANS_INVALID_PARAM);
}

/**
 * @tc.name: GetTargetDeviceStatus_00001
 * @tc.desc: Test GetTargetDeviceStatus, deviceType is empty, except is ERR_ANS_INVALID_PARAM
 * @tc.desc: Test GetTargetDeviceStatus, deviceType is not empty, status == inputStatus
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, GetTargetDeviceStatus_00001, Function | SmallTest | Level1)
{
    std::string deviceType = "";
    int32_t inputStatus = 1;
    int32_t status = 0;
    auto ret = advancedNotificationService_->GetTargetDeviceStatus(deviceType, status);
    ASSERT_EQ(ret, ERR_ANS_INVALID_PARAM);
    uint32_t controlFlag = 0;
    deviceType = "testdeviceType";
    ret = advancedNotificationService_->SetTargetDeviceStatus(deviceType, inputStatus);
    ASSERT_EQ(ret, ERR_OK);
    ret = advancedNotificationService_->GetTargetDeviceStatus(deviceType, status);
    ASSERT_EQ(status, inputStatus);
}

/**
 * @tc.name: ClearAllNotificationGroupInfo_00001
 * @tc.desc: Test ClearAllNotificationGroupInfo, deviceType is not empty, except is ERR_OK
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, ClearAllNotificationGroupInfo_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    sptr<Notification> notification = new (std::nothrow) Notification(request);
    auto record = std::make_shared<NotificationRecord>();
    record->request = request;
    record->notification = notification;
    advancedNotificationService_->notificationList_.push_back(record);
    std::string localSwitch = "false";
    advancedNotificationService_->aggregateLocalSwitch_ = true;
    advancedNotificationService_->ClearAllNotificationGroupInfo(localSwitch);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    ASSERT_EQ(advancedNotificationService_->aggregateLocalSwitch_, false);
}

/**
 * @tc.name: RemoveAllNotificationsByBundleName_00001
 * @tc.desc: Test RemoveAllNotificationsByBundleName, except is notificationList_ == 1
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, RemoveAllNotificationsByBundleName_00001, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new (std::nothrow) NotificationRequest();
    sptr<Notification> notification = new (std::nothrow) Notification(request);
    auto bundle = new NotificationBundleOption(TEST_DEFUALT_BUNDLE, NON_SYSTEM_APP_UID);
    auto record1 = std::make_shared<NotificationRecord>();
    record1->request = request;
    record1->notification = notification;
    record1->bundleOption = bundle;
    advancedNotificationService_->notificationList_.push_back(record1);
    auto record2 = nullptr;
    advancedNotificationService_->notificationList_.push_back(record2);
    std::string bundleName = TEST_DEFUALT_BUNDLE;
    int32_t reason = 0;
    ASSERT_EQ(advancedNotificationService_->notificationList_.size(), 2);
    auto ret = advancedNotificationService_->RemoveAllNotificationsByBundleName(bundleName, reason);
    ASSERT_EQ(advancedNotificationService_->notificationList_.size(), 1);
}

/**
 * @tc.name: DistributeOperation_00001
 * @tc.desc: Test DistributeOperation, DistributeOperationParamCheck faild, except is ERR_ANS_INVALID_PARAM
 * @tc.type: FUNC
 * @tc.require: issue
 */
HWTEST_F(AnsPublishServiceTest, DistributeOperation_00001, Function | SmallTest | Level1)
{
    sptr<NotificationOperationInfo> operationInfo = new (std::nothrow) NotificationOperationInfo();
    sptr<OperationCallbackInterface> callback = nullptr;
    auto ret = advancedNotificationService_->DistributeOperation(operationInfo, callback);
    ASSERT_EQ(ret, ERR_ANS_INVALID_PARAM);
    std::string hashCode = "123456";
    operationInfo->SetHashCode(hashCode);
    ret = advancedNotificationService_->DistributeOperation(operationInfo, callback);
    ASSERT_EQ(ret, ERR_ANS_INVALID_PARAM);
}

}  // namespace Notification
}  // namespace OHOS
